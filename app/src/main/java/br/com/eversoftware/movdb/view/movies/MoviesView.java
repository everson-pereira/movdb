package br.com.eversoftware.movdb.view.movies;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.ArrayList;

import br.com.eversoftware.movdb.R;
import br.com.eversoftware.movdb.model.movies.Movie;
import br.com.eversoftware.movdb.presenter.movies.MoviesPresenter;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;

/**
 * Created by everson on 8/28/17.
 */

public class MoviesView {

    View view;

    MoviesAdapter adapter;

    @BindView(R.id.activity_list_recycleview)
    RecyclerView moviesRecycler;

    private EndlessRecyclerViewScrollListener scrollListener;

    private int state = 0;

    public MoviesView(MoviesActivity moviesContext) {

        FrameLayout parent = new FrameLayout(moviesContext);
        parent.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        view = LayoutInflater.from(moviesContext).inflate(R.layout.activity_movies, parent, true);

        ButterKnife.bind(this, view);

        adapter = new MoviesAdapter();
        moviesRecycler.setAdapter(adapter);

        GridLayoutManager gridLayoutManager =  new GridLayoutManager(moviesContext, 3);
        moviesRecycler.setLayoutManager(gridLayoutManager);

        scrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                if(state == 0)
                    moviesContext.presenter.search(moviesContext.presenter.lastQuery, page);
                if(state == 1)
                    moviesContext.presenter.playing(page);
                if(state == 2)
                    moviesContext.presenter.popular(page);
                if(state == 3)
                    moviesContext.presenter.rated(page);
            }
        };

        moviesRecycler.addOnScrollListener(scrollListener);

    }

    public Observable<Integer> itemClicks()
    {
        return adapter.observeClicks();
    }

    public View view() {
        return view;
    }

    public void swapAdapter(ArrayList<Movie> movies) {
        scrollListener.resetState();
        adapter.swapAdapter(movies);
        adapter.updateAdapterRangeRemove();
    }

    public void updateAdapter(ArrayList<Movie> movies) {
        adapter.updateAdapter(movies);
        adapter.updateAdapterRangeInsert();
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
