package br.com.eversoftware.movdb.view.gallery;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import br.com.eversoftware.movdb.R;
import br.com.eversoftware.movdb.model.gallery.Image;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;


/**
 * Created by everson on 8/28/2017.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryViewHolder> {

    private final PublishSubject<Integer> itemClicks = PublishSubject.create();

    ArrayList<Image> listImages = new ArrayList<>();


    public void swapAdapter(ArrayList<Image> images)  {
        this.listImages.clear();
        this.listImages.addAll(images);
        notifyDataSetChanged();

    }

    public Observable<Integer> observeClicks() {
        return itemClicks;
    }


    @Override
    public GalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_item, parent, false);
        return new GalleryViewHolder(view ,itemClicks);
    }

    @Override
    public void onBindViewHolder(GalleryViewHolder holder, int position) {
        Image image = listImages.get(position);
        holder.bind(image);
    }


    @Override
    public int getItemCount() {
        if (listImages != null && listImages.size() > 0) {
            return listImages.size();
        } else {
            return 0;
        }
    }
}
