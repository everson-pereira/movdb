package br.com.eversoftware.movdb.view.movies;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import br.com.eversoftware.movdb.R;
import br.com.eversoftware.movdb.model.movies.Movie;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;


/**
 * Created by everson on 8/28/2017.
 */

public class MoviesAdapter extends RecyclerView.Adapter<MoviesViewHolder> {

    private final PublishSubject<Integer> itemClicks = PublishSubject.create();

    ArrayList<Movie> listMovies = new ArrayList<>();


    public void swapAdapter(ArrayList<Movie> movies)  {
        this.listMovies.clear();
        this.listMovies.addAll(movies);
        notifyDataSetChanged();

    }

    public void updateAdapter(ArrayList<Movie> movies)  {
        this.listMovies.addAll(movies);
        notifyDataSetChanged();
    }

    public void updateAdapterRangeInsert()  {
        notifyItemRangeInserted(getItemCount(), listMovies.size() - 1);
    }

    public void updateAdapterRangeRemove()  {
        notifyItemRangeRemoved(getItemCount(), listMovies.size() - 1);
    }

    public Observable<Integer> observeClicks() {
        return itemClicks;
    }


    @Override
    public MoviesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_item, parent, false);
        return new MoviesViewHolder(view ,itemClicks);
    }

    @Override
    public void onBindViewHolder(MoviesViewHolder holder, int position) {
        Movie movie = listMovies.get(position);
        holder.bind(movie);
    }


    @Override
    public int getItemCount() {
        if (listMovies != null && listMovies.size() > 0) {
            return listMovies.size();
        } else {
            return 0;
        }
    }
}
