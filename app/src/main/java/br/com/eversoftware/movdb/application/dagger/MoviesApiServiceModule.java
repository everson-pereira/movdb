package br.com.eversoftware.movdb.application.dagger;

import br.com.eversoftware.movdb.api.TMDbAPI;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by everson on 8/29/2017.
 */

@Module
public class MoviesApiServiceModule {

    private static final String BASE_URL = "http://api.themoviedb.org/3/";;

    @AppScope
    @Provides
    TMDbAPI provideApiService(OkHttpClient client, GsonConverterFactory gson, RxJava2CallAdapterFactory rxAdapter) {
        Retrofit retrofit =   new Retrofit.Builder()
                .client(client)
                .baseUrl(BASE_URL)
                .addConverterFactory(gson)
                .addCallAdapterFactory(rxAdapter)
                .build();

        return  retrofit.create(TMDbAPI.class);
    }


}
