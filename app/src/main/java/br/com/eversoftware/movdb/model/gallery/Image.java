package br.com.eversoftware.movdb.model.gallery;

/**
 * Created by everson on 8/31/17.
 */

public class Image {

    private String file_path;

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }
}
