package br.com.eversoftware.movdb.application.dagger;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by everson on 8/29/2017.
 */

@Module
public class AppContextModule {


    private final Context context;

    public AppContextModule(Context aContext) {
        this.context = aContext;
    }

    @AppScope
    @Provides
    Context provideAppContext() {
        return context;
    }

}
