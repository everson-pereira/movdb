package br.com.eversoftware.movdb.model.movies;


import java.util.List;

/**
 * Created by everson on 8/29/17.
 */

public class MoviesResponse {

    private List<Movie> results;

    public List<Movie> getResults() {
        return results;
    }

    public void setResults(List<Movie> results) {
        this.results = results;
    }
}
