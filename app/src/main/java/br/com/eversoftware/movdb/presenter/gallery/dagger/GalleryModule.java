package br.com.eversoftware.movdb.presenter.gallery.dagger;

import br.com.eversoftware.movdb.api.TMDbAPI;
import br.com.eversoftware.movdb.application.utils.rx.RxSchedulers;
import br.com.eversoftware.movdb.model.gallery.GalleryModel;
import br.com.eversoftware.movdb.model.movies.MoviesModel;
import br.com.eversoftware.movdb.presenter.gallery.GalleryPresenter;
import br.com.eversoftware.movdb.presenter.movies.MoviesPresenter;
import br.com.eversoftware.movdb.view.gallery.GalleryActivity;
import br.com.eversoftware.movdb.view.gallery.GalleryView;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by everson on 8/31/17.
 */

@Module
public class GalleryModule {

    GalleryActivity galleyContext;

    public GalleryModule(GalleryActivity context) {
        this.galleyContext = context;
    }

    @GalleryScope
    @Provides
    GalleryView provideView() {
        return new GalleryView(galleyContext);
    }

    @GalleryScope
    @Provides
    GalleryPresenter providePresenter(RxSchedulers schedulers, GalleryView view, GalleryModel model) {
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        return new GalleryPresenter(schedulers, model, view, compositeDisposable);
    }

    @GalleryScope
    @Provides
    GalleryActivity provideContext() {
        return galleyContext;
    }

    @GalleryScope
    @Provides
    GalleryModel provideModel(TMDbAPI api) {
        return new GalleryModel(galleyContext, api);
    }

}
