package br.com.eversoftware.movdb.model.details;

import br.com.eversoftware.movdb.api.TMDbAPI;
import br.com.eversoftware.movdb.application.utils.NetworkUtils;
import br.com.eversoftware.movdb.view.details.DetailsActivity;
import io.reactivex.Observable;


/**
 * Created by everson on 8/28/17.
 */

public class DetailsModel {

    DetailsActivity context;

    TMDbAPI api;

    public DetailsModel(DetailsActivity detailsContext, TMDbAPI api) {
        this.api = api;
        this.context = detailsContext;
    }

    public Observable<Detail> provideDetails(String movieId) {
        return api.detail(movieId, "b5c2b706999065af1f720d5951b53c9a", "pt-BR");
    }

    public Observable<Boolean> isNetworkAvailable() {
        return NetworkUtils.isNetworkAvailableObservable(context);
    }

}
