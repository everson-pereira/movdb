package br.com.eversoftware.movdb.application.utils.rx;


import io.reactivex.Scheduler;

/**
 * Created by everson on 8/29/2017.
 */

public interface RxSchedulers {


    Scheduler runOnBackground();

    Scheduler io();

    Scheduler compute();

    Scheduler androidThread();

    Scheduler internet();



}
