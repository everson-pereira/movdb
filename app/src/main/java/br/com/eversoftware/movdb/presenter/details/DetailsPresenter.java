package br.com.eversoftware.movdb.presenter.details;

import br.com.eversoftware.movdb.application.utils.rx.RxSchedulers;
import br.com.eversoftware.movdb.model.details.Detail;
import br.com.eversoftware.movdb.model.details.DetailsModel;

import br.com.eversoftware.movdb.view.details.DetailsView;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by everson on 8/29/17.
 */

public class DetailsPresenter {

    Detail response;

    DetailsModel model;

    DetailsView view;

    RxSchedulers rxSchedulers;

    CompositeDisposable composite;

    public DetailsPresenter(RxSchedulers schedulers, DetailsModel model, DetailsView view, CompositeDisposable composite) {
        this.rxSchedulers = schedulers;
        this.model = model;
        this.view = view;
        this.composite = composite;
    }

    public void onCreate(String movieId) {
        composite.add(model.provideDetails(movieId)
                .observeOn(rxSchedulers.androidThread())
                .subscribeOn(rxSchedulers.internet())
                .subscribe(this::handleResponse, this::handleError)
        );
    }

    public void onDestroy() {
        composite.clear();
    }

    private void handleResponse(Detail response) {
        this.response = response;
        displayDetails(this.response);
    }

    private void handleError(Throwable error) {

    }

    private void displayDetails(Detail response) {
        this.view.displayBackground(response.getPoster_path());
        this.view.displayTitle(response.getTitle());
        this.view.displayOriginal(response.getOriginal_title());
        this.view.displayYear(response.getRelease_date().split("-")[0]);
        this.view.displayTagLine(response.getTagline());
        this.view.displayOverview(response.getOverview());
    }

}
