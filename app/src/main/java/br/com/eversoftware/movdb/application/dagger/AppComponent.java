package br.com.eversoftware.movdb.application.dagger;

import android.content.Context;

import br.com.eversoftware.movdb.api.TMDbAPI;
import br.com.eversoftware.movdb.application.utils.rx.RxSchedulers;
import dagger.Component;

/**
 * Created by everson on 8/29/2017.
 */

@AppScope
@Component(modules = {NetworkModule.class, AppContextModule.class, RxModule.class, MoviesApiServiceModule.class})
public interface AppComponent {


    Context getAppContext();

    RxSchedulers rxSchedulers();

    TMDbAPI apiService();

}
