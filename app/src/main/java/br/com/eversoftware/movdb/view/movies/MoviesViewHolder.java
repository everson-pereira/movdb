package br.com.eversoftware.movdb.view.movies;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import br.com.eversoftware.movdb.R;
import br.com.eversoftware.movdb.application.AppBuilder;
import br.com.eversoftware.movdb.model.movies.Movie;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by everson on 8/28/2017.
 */

public class MoviesViewHolder extends ViewHolder {

    View view;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.poster)
    ImageView poster;

    @BindView(R.id.subtitle)
    TextView subtitle;

    public MoviesViewHolder(View itemView, PublishSubject<Integer> clickSubject) {
        super(itemView);
        this.view = itemView;
        ButterKnife.bind(this, view);
        view.setOnClickListener(v -> clickSubject.onNext(getAdapterPosition()));
    }

    void bind(Movie movie) {
        title.setText(TextUtils.isEmpty(movie.getTitle()) ? "no title" : movie.getTitle());
        String path = AppBuilder.getConfig().getBase_url() + "w154" + movie.getPoster_path();
        AppBuilder.PICASSO.load(path).into(poster);
        subtitle.setText(movie.getRelease_date().split("-")[0]);
    }

}
