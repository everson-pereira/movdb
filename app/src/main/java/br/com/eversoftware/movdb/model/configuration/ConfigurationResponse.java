package br.com.eversoftware.movdb.model.configuration;

/**
 * Created by everson on 8/30/17.
 */

public class ConfigurationResponse {

    private Configuration images;

    public Configuration getConfig() {
        return images;
    }

    public void setConfig(Configuration images) {
        this.images = images;
    }
}
