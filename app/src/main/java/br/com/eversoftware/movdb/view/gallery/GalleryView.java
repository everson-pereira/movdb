package br.com.eversoftware.movdb.view.gallery;

import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.ArrayList;

import br.com.eversoftware.movdb.R;
import br.com.eversoftware.movdb.model.gallery.Image;
import br.com.eversoftware.movdb.model.movies.Movie;
import br.com.eversoftware.movdb.view.movies.MoviesAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;

/**
 * Created by everson on 8/31/17.
 */

public class GalleryView {

    View view;

    GalleryAdapter adapter;

    @BindView(R.id.activity_list_recycleview)
    RecyclerView imagesRecycler;

    public GalleryView(GalleryActivity galleyContext) {
        FrameLayout layout = new FrameLayout(galleyContext);
        layout.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        view = LayoutInflater.from(galleyContext).inflate(R.layout.activity_gallery,layout,true);
        ButterKnife.bind(this,view);

        adapter = new GalleryAdapter();
        imagesRecycler.setAdapter(adapter);

        LinearLayoutManager layoutManager =  new LinearLayoutManager(galleyContext);
        imagesRecycler.setLayoutManager(layoutManager);
    }

    public Observable<Integer> itemClicks() {
        return adapter.observeClicks();
    }

    public void swapAdapter(ArrayList<Image> images) {
        adapter.swapAdapter(images);
    }

    public View view() {
        return view;
    }
}

