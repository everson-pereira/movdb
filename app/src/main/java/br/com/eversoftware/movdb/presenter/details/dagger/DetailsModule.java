package br.com.eversoftware.movdb.presenter.details.dagger;

import br.com.eversoftware.movdb.api.TMDbAPI;
import br.com.eversoftware.movdb.application.utils.rx.RxSchedulers;
import br.com.eversoftware.movdb.model.details.DetailsModel;
import br.com.eversoftware.movdb.presenter.details.DetailsPresenter;
import br.com.eversoftware.movdb.view.details.DetailsView;
import br.com.eversoftware.movdb.view.details.DetailsActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by everson on 8/29/2017.
 */

@Module
public class DetailsModule {

    DetailsActivity detailsContext;

    public DetailsModule(DetailsActivity context) {
        this.detailsContext = context;
    }

    @DetailsScope
    @Provides
    DetailsView provideView() {
        return  new DetailsView(detailsContext);
    }

    @DetailsScope
    @Provides
    DetailsPresenter providePresenter(RxSchedulers schedulers, DetailsView view, DetailsModel model) {
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        return new DetailsPresenter(schedulers, model, view, compositeDisposable);
    }

    @DetailsScope
    @Provides
    DetailsActivity provideContext() {
        return detailsContext;
    }

    @DetailsScope
    @Provides
    DetailsModel provideModel(TMDbAPI api) {
        return new DetailsModel(detailsContext, api);
    }
}
