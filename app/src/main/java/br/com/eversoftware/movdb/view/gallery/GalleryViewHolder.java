package br.com.eversoftware.movdb.view.gallery;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import br.com.eversoftware.movdb.R;
import br.com.eversoftware.movdb.application.AppBuilder;
import br.com.eversoftware.movdb.model.gallery.Image;
import br.com.eversoftware.movdb.model.movies.Movie;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by everson on 8/31/17.
 */

class GalleryViewHolder extends RecyclerView.ViewHolder {

    View view;

    @BindView(R.id.image)
    ImageView image;

    public GalleryViewHolder(View itemView, PublishSubject<Integer> clickSubject) {
        super(itemView);
        this.view = itemView;
        ButterKnife.bind(this, view);
//        view.setOnClickListener(v -> clickSubject.onNext(getAdapterPosition()));
    }

    void bind(Image image) {
        String path = AppBuilder.getConfig().getBase_url() + "w780" + image.getFile_path();
        AppBuilder.PICASSO.load(path).into(this.image);
    }
}
