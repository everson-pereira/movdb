package br.com.eversoftware.movdb.application.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by everson on 8/29/2017.
 */

@Scope
@Retention(RetentionPolicy.CLASS)
@interface AppScope {
}
