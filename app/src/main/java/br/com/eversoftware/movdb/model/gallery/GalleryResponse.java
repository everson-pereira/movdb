package br.com.eversoftware.movdb.model.gallery;

import java.util.List;

/**
 * Created by everson on 8/31/17.
 */

public class GalleryResponse {

    private List<Image> backdrops;
    private List<Image> posters;

    public List<Image> getBackdrops() {
        return backdrops;
    }

    public void setBackdrops(List<Image> backdrops) {
        this.backdrops = backdrops;
    }

    public List<Image> getPosters() {
        return posters;
    }

    public void setPosters(List<Image> posters) {
        this.posters = posters;
    }
}