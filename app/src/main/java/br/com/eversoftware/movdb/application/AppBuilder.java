package br.com.eversoftware.movdb.application;

import android.app.Application;

import com.squareup.picasso.Picasso;

import br.com.eversoftware.movdb.application.dagger.AppComponent;
import br.com.eversoftware.movdb.application.dagger.AppContextModule;
import br.com.eversoftware.movdb.application.dagger.DaggerAppComponent;
import br.com.eversoftware.movdb.model.configuration.Configuration;
import br.com.eversoftware.movdb.model.configuration.ConfigurationResponse;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by everson on 8/29/17.
 */

public class AppBuilder extends Application{

    private static AppComponent appComponent;

    private static Configuration config;

    public static Picasso PICASSO;

    @Override
    public void onCreate() {
        super.onCreate();
        buildAppComponent();
        buildConfiguration();
        PICASSO = new Picasso.Builder(this).build();

    }

    private void buildAppComponent() {
        appComponent = DaggerAppComponent.builder().appContextModule(new AppContextModule(this)).build();

    }

    private void buildConfiguration() {
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        compositeDisposable.add(appComponent.apiService().configuration("b5c2b706999065af1f720d5951b53c9a")
                .observeOn(appComponent.rxSchedulers().androidThread())
                .subscribeOn(appComponent.rxSchedulers().internet())
                .subscribe(this::handleResponse, this::handleError)
        );


    }

    private void handleResponse(ConfigurationResponse response) {
        config = response.getConfig();
    }

    private void handleError(Throwable error) {

    }

    public static AppComponent getNetComponent() {
        return appComponent;
    }

    public static Configuration getConfig() {
        return config;
    }

}
