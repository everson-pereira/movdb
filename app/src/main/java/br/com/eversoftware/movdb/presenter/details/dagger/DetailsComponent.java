package br.com.eversoftware.movdb.presenter.details.dagger;

import br.com.eversoftware.movdb.application.dagger.AppComponent;
import br.com.eversoftware.movdb.view.details.DetailsActivity;
import dagger.Component;

/**
 * Created by everson on 8/29/2017.
 */

@DetailsScope
@Component(dependencies = {AppComponent.class}, modules = {DetailsModule.class})
public interface DetailsComponent {

    void inject(DetailsActivity context);

}
