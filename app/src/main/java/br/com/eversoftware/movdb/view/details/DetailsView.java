package br.com.eversoftware.movdb.view.details;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import br.com.eversoftware.movdb.R;
import br.com.eversoftware.movdb.application.AppBuilder;
import br.com.eversoftware.movdb.model.details.Detail;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by everson on 8/29/17.
 */

public class DetailsView {

    @BindView(R.id.background)
    ImageView background;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.year)
    TextView year;

    @BindView(R.id.original_title)
    TextView original;

    @BindView(R.id.overview_container)
    View overviewContainer;

    @BindView(R.id.overview)
    TextView overview;

    @BindView(R.id.tagline_container)
    View taglineContainer;

    @BindView(R.id.tagline)
    TextView tagline;


    View view;

    public DetailsView(DetailsActivity activity)
    {
        FrameLayout layout = new FrameLayout(activity);
        layout.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        view = LayoutInflater.from(activity).inflate(R.layout.activity_details,layout,true);
        ButterKnife.bind(this,view);
    }

    public View view()
    {
        return view;
    }

    public void displayTitle(String title) {
        if(!title.isEmpty())
            this.title.setText(title);
        else
            this.title.setText("===");
    }

    public void displayYear(String year) {
        if(!year.isEmpty())
            this.year.setText(year);
        else
            this.year.setVisibility(View.GONE);
    }

    public void displayOriginal(String original) {
        if(!original.isEmpty())
            this.original.setText(original);
        else
            this.original.setVisibility(View.GONE);
    }

    public void displayOverview(String overview) {
        if(!overview.isEmpty()){
            this.overviewContainer.setVisibility(View.VISIBLE);
            this.overview.setText(overview);
        }
        else {
            this.overviewContainer.setVisibility(View.GONE);
        }
    }

    public void displayTagLine(String tagline) {
        if(!tagline.isEmpty()) {
            this.taglineContainer.setVisibility(View.VISIBLE);
            this.tagline.setText(tagline);
        }
        else {
            this.taglineContainer.setVisibility(View.GONE);
        }
    }

    public void displayBackground(String path) {
        AppBuilder.PICASSO.load(AppBuilder.getConfig().getBase_url() + "original" + path).into(background);
    }

}
