package br.com.eversoftware.movdb.presenter.movies.dagger;

import br.com.eversoftware.movdb.api.TMDbAPI;
import br.com.eversoftware.movdb.application.utils.rx.RxSchedulers;
import br.com.eversoftware.movdb.model.movies.MoviesModel;
import br.com.eversoftware.movdb.presenter.movies.MoviesPresenter;
import br.com.eversoftware.movdb.view.movies.MoviesActivity;
import br.com.eversoftware.movdb.view.movies.MoviesView;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;


/**
 * Created by everson on 8/28/17.
 */

@Module
public class MoviesModule {

    MoviesActivity moviesListContext;

    public MoviesModule(MoviesActivity context) {
        this.moviesListContext = context;
    }

    @MoviesScope
    @Provides
    MoviesView provideView() {
        return new MoviesView(moviesListContext);
    }

    @MoviesScope
    @Provides
    MoviesPresenter providePresenter(RxSchedulers schedulers, MoviesView view, MoviesModel model) {
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        return new MoviesPresenter(schedulers, model, view, compositeDisposable);
    }

    @MoviesScope
    @Provides
    MoviesActivity provideContext() {
        return moviesListContext;
    }

    @MoviesScope
    @Provides
    MoviesModel provideModel(TMDbAPI api) {
        return new MoviesModel(moviesListContext, api);
    }

}
