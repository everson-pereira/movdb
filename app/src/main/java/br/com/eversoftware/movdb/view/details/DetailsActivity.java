package br.com.eversoftware.movdb.view.details;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import javax.inject.Inject;

import br.com.eversoftware.movdb.R;
import br.com.eversoftware.movdb.application.AppBuilder;
import br.com.eversoftware.movdb.presenter.details.DetailsPresenter;
import br.com.eversoftware.movdb.presenter.details.dagger.DaggerDetailsComponent;
import br.com.eversoftware.movdb.presenter.details.dagger.DetailsModule;
import br.com.eversoftware.movdb.view.gallery.GalleryActivity;

/**
 * Created by everson on 8/29/17.
 */

public class DetailsActivity extends AppCompatActivity {

    @Inject
    DetailsView view;

    @Inject
    DetailsPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String movieId = (String) getIntent().getExtras().get("movieId");

        DaggerDetailsComponent.builder().appComponent(AppBuilder.getNetComponent()).detailsModule(new DetailsModule(this)).build().inject(this);

        setContentView(view.view());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(view.getContext(), GalleryActivity.class);
                in.putExtra("movieId", movieId);
                startActivity(in);
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter.onCreate(movieId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }
}
