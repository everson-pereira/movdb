package br.com.eversoftware.movdb.presenter.gallery.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by everson on 8/31/17.
 */

@Scope
@Retention(RetentionPolicy.CLASS)
@interface GalleryScope {
}
