package br.com.eversoftware.movdb.application.dagger;

import br.com.eversoftware.movdb.application.utils.rx.AppRxSchedulers;
import br.com.eversoftware.movdb.application.utils.rx.RxSchedulers;
import dagger.Module;
import dagger.Provides;


/**
 * Created by everson on 8/29/2017.
 */

@Module
public class RxModule {

    @Provides
    RxSchedulers provideRxSchedulers() {
        return new AppRxSchedulers();
    }
}
