package br.com.eversoftware.movdb.model.movies;

import br.com.eversoftware.movdb.api.TMDbAPI;
import br.com.eversoftware.movdb.application.utils.NetworkUtils;
import br.com.eversoftware.movdb.view.movies.MoviesActivity;
import io.reactivex.Observable;

/**
 * Created by everson on 8/28/17.
 */

public class MoviesModel {

    MoviesActivity context;

    TMDbAPI api;

    public MoviesModel(MoviesActivity moviesListContext, TMDbAPI api) {
        this.api = api;
        this.context = moviesListContext;
    }

    public Observable<MoviesResponse> provideSearchMovies(String query, int page) {
        return api.search("b5c2b706999065af1f720d5951b53c9a", "pt-BR", page, query);
    }

    public Observable<MoviesResponse> providePlayingMovies(int page) {
        return api.playing("b5c2b706999065af1f720d5951b53c9a", "pt-BR", page);
    }

    public Observable<MoviesResponse> providePopularMovies(int page) {
        return api.popular("b5c2b706999065af1f720d5951b53c9a", "pt-BR", page);
    }

    public Observable<MoviesResponse> provideRatedMovies(int page) {
        return api.rated("b5c2b706999065af1f720d5951b53c9a", "pt-BR", page);
    }

    public Observable<Boolean> isNetworkAvailable() {
        return NetworkUtils.isNetworkAvailableObservable(context);
    }

    public void goToDetailsActivity(String movieId) {
        context.goToDetailsActivity(movieId);
    }


}
