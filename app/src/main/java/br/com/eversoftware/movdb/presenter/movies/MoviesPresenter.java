package br.com.eversoftware.movdb.presenter.movies;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import br.com.eversoftware.movdb.application.utils.rx.RxSchedulers;
import br.com.eversoftware.movdb.model.movies.Movie;
import br.com.eversoftware.movdb.model.movies.MoviesModel;
import br.com.eversoftware.movdb.model.movies.MoviesResponse;
import br.com.eversoftware.movdb.view.movies.MoviesView;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


/**
 * Created by everson on 8/28/17.
 */

public class MoviesPresenter {

    MoviesModel model;

    MoviesView view;

    RxSchedulers rxSchedulers;

    CompositeDisposable composite;

    MoviesResponse response = new MoviesResponse();

    ArrayList<Movie> moviesResult = new ArrayList<>();

    public String lastQuery;

    private int page;

    public MoviesPresenter(RxSchedulers schedulers, MoviesModel model, MoviesView view, CompositeDisposable composite) {
        this.rxSchedulers = schedulers;
        this.model = model;
        this.view = view;
        this.composite = composite;
    }

    public void onCreate() {
        composite.add(respondToClick());
        this.view.setState(1);
        this.playing(1);
    }

    public void onDestroy() {
        composite.clear();
    }

    private Disposable respondToClick() {
        return view.itemClicks().subscribe(integer -> model.goToDetailsActivity(moviesResult.get(integer).getId()));
    }

    public void search(String query, int page) {
        this.page = page;
        this.lastQuery = query;
        composite.add(model.provideSearchMovies(query, page)
                .observeOn(rxSchedulers.androidThread())
                .subscribeOn(rxSchedulers.internet())
                .subscribe(this::handleResponse, this::handleError)
        );
    }

    public void playing(int page) {
        this.page = page;
        composite.add(model.providePlayingMovies(page)
                .observeOn(rxSchedulers.androidThread())
                .subscribeOn(rxSchedulers.internet())
                .subscribe(this::handleResponse, this::handleError)
        );
    }

    public void popular(int page) {
        this.page = page;
        composite.add(model.providePopularMovies(page)
                .observeOn(rxSchedulers.androidThread())
                .subscribeOn(rxSchedulers.internet())
                .subscribe(this::handleResponse, this::handleError)
        );
    }
    public void rated(int page) {
        this.page = page;
        composite.add(model.provideRatedMovies(page)
                .observeOn(rxSchedulers.androidThread())
                .subscribeOn(rxSchedulers.internet())
                .subscribe(this::handleResponse, this::handleError)
        );
    }

    private void handleResponse(MoviesResponse response) {

        this.response = response;

        if(page == 1) {
            this.moviesResult.clear();
            this.moviesResult.addAll(response.getResults());
            view.swapAdapter((ArrayList<Movie>) this.response.getResults());
        }
        else {
            this.moviesResult.addAll(response.getResults());
            view.updateAdapter((ArrayList<Movie>) this.response.getResults());
        }
    }

    private void handleError(Throwable error) {

    }


}
