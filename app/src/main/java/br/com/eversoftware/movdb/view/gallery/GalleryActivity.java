package br.com.eversoftware.movdb.view.gallery;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import javax.inject.Inject;

import br.com.eversoftware.movdb.R;
import br.com.eversoftware.movdb.application.AppBuilder;
import br.com.eversoftware.movdb.presenter.gallery.GalleryPresenter;
import br.com.eversoftware.movdb.presenter.gallery.dagger.DaggerGalleryComponent;
import br.com.eversoftware.movdb.presenter.gallery.dagger.GalleryModule;

/**
 * Created by everson on 8/31/17.
 */

public class GalleryActivity extends AppCompatActivity {

    @Inject
    GalleryView view;

    @Inject
    GalleryPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String movieId = (String) getIntent().getExtras().get("movieId");

        DaggerGalleryComponent.builder().appComponent(AppBuilder.getNetComponent()).galleryModule(new GalleryModule(this)).build().inject(this);

        setContentView(view.view());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter.onCreate(movieId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

}
