package br.com.eversoftware.movdb.presenter.movies.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by everson on 8/28/17.
 */
@Scope
@Retention(RetentionPolicy.CLASS)
@interface MoviesScope {
}
