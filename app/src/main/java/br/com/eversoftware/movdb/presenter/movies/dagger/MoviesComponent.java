package br.com.eversoftware.movdb.presenter.movies.dagger;

import br.com.eversoftware.movdb.application.dagger.AppComponent;
import br.com.eversoftware.movdb.view.movies.MoviesActivity;
import dagger.Component;

/**
 * Created by everson on 8/29/17.
 */

@MoviesScope
@Component(dependencies = {AppComponent.class} , modules = {MoviesModule.class})
public interface MoviesComponent {

    void inject (MoviesActivity moviesActivity);
}
