package br.com.eversoftware.movdb.api;


import br.com.eversoftware.movdb.model.configuration.Configuration;
import br.com.eversoftware.movdb.model.configuration.ConfigurationResponse;
import br.com.eversoftware.movdb.model.details.Detail;
import br.com.eversoftware.movdb.model.gallery.GalleryResponse;
import br.com.eversoftware.movdb.model.movies.MoviesResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by everson on 8/29/2017.
 */

public interface TMDbAPI {

    @GET("configuration")
    Observable<ConfigurationResponse> configuration(@Query("api_key") String apiKey);

    @GET("search/movie")
    Observable<MoviesResponse> search(@Query("api_key") String apiKey, @Query("language") String language, @Query("page") int page, @Query("query") String query);

    @GET("movie/now_playing")
    Observable<MoviesResponse> playing(@Query("api_key") String apiKey, @Query("language") String language, @Query("page") int page);

    @GET("movie/popular")
    Observable<MoviesResponse> popular(@Query("api_key") String apiKey, @Query("language") String language, @Query("page") int page);

    @GET("movie/top_rated")
    Observable<MoviesResponse> rated(@Query("api_key") String apiKey, @Query("language") String language, @Query("page") int page);

    @GET("movie/{id}")
    Observable<Detail> detail(@Path("id") String id, @Query("api_key") String key, @Query("language") String language);

    @GET("movie/{id}/images")
    Observable<GalleryResponse> images( @Path("id") String movieID, @Query("api_key") String apiKey);

}
