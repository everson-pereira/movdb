package br.com.eversoftware.movdb.presenter.gallery.dagger;

import br.com.eversoftware.movdb.application.dagger.AppComponent;
import br.com.eversoftware.movdb.view.gallery.GalleryActivity;
import br.com.eversoftware.movdb.view.movies.MoviesActivity;
import dagger.Component;

/**
 * Created by everson on 8/31/17.
 */

@GalleryScope
@Component(dependencies = {AppComponent.class} , modules = {GalleryModule.class})
public interface GalleryComponent {
    void inject (GalleryActivity galleryActivity);
}
