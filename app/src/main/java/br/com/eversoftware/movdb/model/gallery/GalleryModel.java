package br.com.eversoftware.movdb.model.gallery;

import br.com.eversoftware.movdb.api.TMDbAPI;
import br.com.eversoftware.movdb.view.gallery.GalleryActivity;
import io.reactivex.Observable;

/**
 * Created by everson on 8/31/17.
 */

public class GalleryModel {

    GalleryActivity context;

    TMDbAPI api;

    public GalleryModel(GalleryActivity galleyContext, TMDbAPI api) {
        this.api = api;
        this.context = galleyContext;
    }

    public Observable<GalleryResponse> provideImages(String movieId) {
        return api.images(movieId, "b5c2b706999065af1f720d5951b53c9a");
    }

    public void goToImageActivity(String movieId) {
//        context.goToDetailsActivity(movieId);
    }
}
