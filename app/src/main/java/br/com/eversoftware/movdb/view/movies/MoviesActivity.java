package br.com.eversoftware.movdb.view.movies;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import br.com.eversoftware.movdb.R;
import br.com.eversoftware.movdb.application.AppBuilder;
import br.com.eversoftware.movdb.presenter.movies.MoviesPresenter;
import br.com.eversoftware.movdb.presenter.movies.dagger.DaggerMoviesComponent;
import br.com.eversoftware.movdb.presenter.movies.dagger.MoviesModule;
import br.com.eversoftware.movdb.view.details.DetailsActivity;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by everson on 8/28/17.
 */

public class MoviesActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    @Inject
    MoviesView view;

    @Inject
    MoviesPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerMoviesComponent.builder().appComponent(AppBuilder.getNetComponent()).moviesModule(new MoviesModule(this)).build().inject(this);

        setContentView(view.view());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        presenter.onCreate();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        MenuItem mSearchMenuItem = menu.findItem(R.id.search_view);
        SearchView searchView = (SearchView) mSearchMenuItem.getActionView();
        fromSearchView(searchView)
                .debounce(350, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(query -> {
                    view.setState(0);
                    presenter.search(query, 1);
                });
        return true;
    }

    public Observable<String> fromSearchView(@NonNull final SearchView searchView) {

        final BehaviorSubject<String> subject = BehaviorSubject.create();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!query.isEmpty()) {
                    subject.onNext(query);
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!newText.isEmpty()) {
                    subject.onNext(newText);
                }
                return true;
            }
        });

        return subject;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_playing) {
            view.setState(1);
            presenter.playing(1);
        } else if (id == R.id.nav_popular) {
            view.setState(2);
            presenter.popular(1);
        } else if (id == R.id.nav_top_rated) {
            view.setState(3);
            presenter.rated(1);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void goToDetailsActivity(String movieId) {
        Intent in = new Intent(this, DetailsActivity.class);
        in.putExtra("movieId", movieId);
        startActivity(in);
    }

}
