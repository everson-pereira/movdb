package br.com.eversoftware.movdb.presenter.gallery;

import java.util.ArrayList;

import br.com.eversoftware.movdb.application.utils.rx.RxSchedulers;
import br.com.eversoftware.movdb.model.gallery.GalleryModel;
import br.com.eversoftware.movdb.model.gallery.GalleryResponse;
import br.com.eversoftware.movdb.model.gallery.Image;
import br.com.eversoftware.movdb.view.gallery.GalleryView;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by everson on 8/31/17.
 */

public class GalleryPresenter{

    GalleryModel model;

    GalleryView view;

    RxSchedulers rxSchedulers;

    CompositeDisposable composite;

    GalleryResponse response = new GalleryResponse();

    ArrayList<Image> imagesResult = new ArrayList<>();

    public GalleryPresenter(RxSchedulers schedulers, GalleryModel model, GalleryView view, CompositeDisposable compositeDisposable) {
        this.rxSchedulers = schedulers;
        this.model = model;
        this.view = view;
        this.composite = compositeDisposable;
    }

    public void onCreate(String movieId) {
        this.images(movieId);
    }

    public void onDestroy() {
        composite.clear();
    }

//    private Disposable respondToClick() {
////        return view.itemClicks().subscribe(integer -> model.goToImageActivity(imagesResult.get(integer).getId()));
//    }

    public void images(String movieId) {
        this.composite.add(model.provideImages(movieId)
                .observeOn(rxSchedulers.androidThread())
                .subscribeOn(rxSchedulers.internet())
                .subscribe(this::handleResponse, this::handleError)
        );
    }

    private void handleResponse(GalleryResponse response) {

        this.response = response;
        this.imagesResult.addAll(this.response.getBackdrops());
        view.swapAdapter((ArrayList<Image>) this.imagesResult);
    }

    private void handleError(Throwable error) {

    }
}

